package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CounterTest {
    @Test
    public void testCounterOne() throws Exception {

        int value = new Counter().dmethod(1,0);
        assertEquals("Count One", Integer.MAX_VALUE , value);
     
    }

    @Test
    public void testCounterTwo() throws Exception {

        int value = new Counter().dmethod(1,1);
        assertEquals("Count One", 1 , value);
     
    }

}
