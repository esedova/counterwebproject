package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class AboutTest {
    @Test
    public void testAbout() throws Exception {

        String text = new About().desc();
        assertEquals("About Text", "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!", text);
     
    }

}
